#Доопрацюйте класс Triangle з попередньої домашки наступним чином:
#обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=)
#print() обʼєкту классу Triangle показує координати його вершин

import math


class Point:
    _x = None
    _y = None

    @property  # getter
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, int):
            raise TypeError
        self._x = value

    @property  # getter
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, int):
            raise TypeError
        self._y = value

    def __init__(self, x_coord, y_coord):
        self.x = x_coord
        self.y = y_coord


class Line:
    begin = None
    end = None

    def __init__(self, begin_point: Point, end_point: Point):
        self.begin = begin_point
        self.end = end_point

    @property
    def length(self):
        # print('in length_getter')
        return ((self.begin.x - self.end.x) ** 2 + (self.begin.y - self.end.y) ** 2) ** 0.5


class Triangle:

    def __init__(self, a: Point, b: Point, c: Point):
        self.a = a
        self.b = b
        self.c = c
        self.ab = Line(a, b)
        self.bc = Line(b, c)
        self.ac = Line(a, c)

    def __str__(self):
        return f'Triangle vertices {self.a} and  {self.b} and {self.c}'

    def __repr__(self):
        return f'vertices({self.a}, {self.b}, {self.c})'

    @property
    def half_perimetr(self):
        return (self.ab.length + self.ac.length + self.bc.length) / 2

    @property
    def square(self):
        return math.sqrt(
            self.half_perimetr * (self.half_perimetr - self.ab.length) * (self.half_perimetr - self.ac.length) *
            (self.half_perimetr - self.bc.length))

    def __gt__(self, other):
        return self.square > other.square

    def __eq__(self, other):
        return self.square == other.square

    def __ne__ (self, other):
        return self.square != other.square

    def __lt__(self, other):
        return self.square < other.square

    def __le__(self, other):
        return self.square <= other.square

    def __ge__(self, other):
        return self.square >= other.square


if __name__ == '__main__':
    point_a = Point(1, 1)
    point_b = Point(3, 3)
    point_c = Point(1, 5)

    point_a2 = Point(1, 0)
    point_b2 = Point(3,4)
    point_c2 = Point(1,2)



    triangle = Triangle(point_a, point_b, point_c)
    print(triangle.ab.length, triangle.ac.length, triangle.bc.length)
    print(triangle.half_perimetr)
    print(triangle.square)

    triangle2 = Triangle(point_a, point_b, point_c)
    print(triangle == triangle2)

    triangle3 = Triangle(point_a2, point_b2, point_c2)
    print(triangle >= triangle3)

    triangle4 = Triangle(point_a2, point_b2, point_c2)
    print(triangle != triangle4)

    triangle5 = Triangle(point_a2, point_b2, point_c2)
    print(triangle < triangle5)

    print(triangle.__str__())
